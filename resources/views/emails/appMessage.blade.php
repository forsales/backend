<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h3 style="text-decoration: underline; color: #db4437;">NEW EMAIL FROM FOR SALES APPLICATION</h3><br>
        <h4><span style="text-decoration: underline; color: #db4437;">Name:</span> <span style="color: #1da1f2;">{{ $name }}</span></h4>
        <h4><span style="text-decoration: underline; color: #db4437;">Email:</span> <span style="color: #1da1f2;">{{ $email }}</span></h4>
        <h4 style="text-decoration: underline; color: #db4437">Message:</h4>
        <h4><span style="color: #1da1f2;">{{ $body }}</span></h4>
    </div>
</div>
</body>
</html>
