import axios from 'axios'
import {apiMethods} from './apiConfig'

function getHeader() {
    var token = JSON.parse(localStorage.getItem('token'));
    var header = {headers: {Authorization: token}};
    return header;
}


export function login(formData) {
    return axios.post(apiMethods.login, formData);
}
export function getAdmin() {
    return axios.get(apiMethods.getAdmin,getHeader());
}
export function addAdmin(formData) {
    return axios.post(apiMethods.addAdmin, formData, getHeader());
}
export function updateAdmin(formData) {
    return axios.post(apiMethods.updateAdmin, formData, getHeader());
}
export function updateAdminPassword(formData) {
    return axios.post(apiMethods.updateAdminPassword, formData, getHeader());
}
export function homeInfo() {
    return axios.get(apiMethods.homeInfo, getHeader());
}

export function getSetting() {
    return axios.get(apiMethods.getSetting);
}
export function updateSetting(formData) {
    return axios.post(apiMethods.updateSetting, formData, getHeader());
}

export function deleteMedia(queryParams) {
    return axios.delete(apiMethods.deleteMedia+queryParams, getHeader());
}

export function getBankAccounts() {
    return axios.get(apiMethods.getBankAccounts);
}
export function addBankAccount(formData) {
    return axios.post(apiMethods.addBankAccount, formData, getHeader());
}
export function updateBankAccount(formData) {
    return axios.post(apiMethods.updateBankAccount, formData, getHeader());
}
export function deleteBankAccount(bankAccount_id) {
    return axios.delete(apiMethods.deleteBankAccount+bankAccount_id, getHeader());
}

export function getAppMessages() {
    return axios.get(apiMethods.getAppMessages, getHeader());
}
export function showAppMessage(appMessage_id) {
    return axios.get(apiMethods.showAppMessage+appMessage_id, getHeader());
}

export function getCities() {
    return axios.get(apiMethods.getCities);
}
export function addCity(formData) {
    return axios.post(apiMethods.addCity, formData, getHeader());
}
export function updateCity(formData) {
    return axios.post(apiMethods.updateCity, formData, getHeader());
}
export function deleteCity(city_id) {
    return axios.delete(apiMethods.deleteCity+city_id, getHeader());
}

export function getCategories(type) {
    return axios.get(apiMethods.getCategories+type);
}
export function showCategory(category_id) {
    return axios.get(apiMethods.showCategory+category_id);
}
export function addCategory(formData) {
    return axios.post(apiMethods.addCategory, formData, getHeader());
}
export function updateCategory(formData) {
    return axios.post(apiMethods.updateCategory, formData, getHeader());
}
export function deleteCategory(category_id) {
    return axios.delete(apiMethods.deleteCategory+category_id, getHeader());
}

export function getSliders() {
    return axios.get(apiMethods.getSliders, getHeader());
}
export function changeSliderStatus(slider_id) {
    return axios.get(apiMethods.changeSliderStatus+slider_id, getHeader());
}
export function addSlider(formData) {
    return axios.post(apiMethods.addSlider, formData, getHeader());
}
export function updateSlider(formData) {
    return axios.post(apiMethods.updateSlider, formData, getHeader());
}
export function deleteSlider(slider_id) {
    return axios.delete(apiMethods.deleteSlider+slider_id, getHeader());
}

export function getAdvertisements(queryParams) {
    return axios.get(apiMethods.getAdvertisements+queryParams);
}
export function getDeletedAdvertisements(queryParams) {
    return axios.get(apiMethods.getDeletedAdvertisements+queryParams, getHeader());
}
export function showAdvertisement(advertisement_id) {
    return axios.get(apiMethods.showAdvertisement+advertisement_id, getHeader());
}
export function updateAdvertisement(FormData) {
    return axios.post(apiMethods.updateAdvertisement, FormData, getHeader());
}
export function deleteAdvertisement(advertisement_id) {
    return axios.delete(apiMethods.deleteAdvertisement+advertisement_id, getHeader());
}

export function deleteComment(comment_id) {
    return axios.delete(apiMethods.deleteComment+comment_id, getHeader());
}

export function getAllConversations() {
    return axios.get(apiMethods.getAllConversations, getHeader());
}
export function showConversation(conversation_id) {
    return axios.get(apiMethods.showConversation+conversation_id, getHeader());
}
export function sendMessage(FormData) {
    return axios.post(apiMethods.sendMessage, FormData,getHeader());
}

export function getUsers() {
    return axios.get(apiMethods.getUsers, getHeader());
}
export function addUser(FormData) {
    return axios.post(apiMethods.addUser, FormData, getHeader());
}
export function updateUser(FormData) {
    return axios.post(apiMethods.updateUser, FormData,getHeader());
}
export function changeUserStatus(user_id) {
    return axios.get(apiMethods.changeUserStatus+user_id ,getHeader());
}
export function deleteUser(FormData) {
    return axios.post(apiMethods.deleteUser, FormData,getHeader());
}

export function reorderCategories(FormData) {
    return axios.post(apiMethods.reorderCategories, FormData,getHeader());
}

export function getComplaints(queryParams) {
    return axios.get(apiMethods.getComplaints+queryParams, getHeader());
}

export function closeComplaint(FormData) {
    return axios.post(apiMethods.closeComplaint, FormData,getHeader());
}
