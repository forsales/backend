// export const appUrl = 'http://127.0.0.1:96/';
export const appUrl = 'https://forsalesapp.com/';

export const client_Id = '2';
export const client_Secret = 'K7ES3pl6J9UDI5gNZxG9FMXFp2Ph12OJkrpJCMxJ';

import Vue from 'vue'
export const EventBus = new Vue();

export const apiMethods = {
    'login': 'oauth/token',
    'getAdmin': 'api/admins/show',
    'addAdmin': 'api/admins/store',
    'updateAdmin': 'api/admins/update',
    'updateAdminPassword': 'api/admins/updatePassword',

    'homeInfo': 'api/home/index',

    'getSetting': 'api/settings/index',
    'updateSetting': 'api/settings/update',

    'deleteMedia': 'api/users/media',

    'getBankAccounts': 'api/bankAccounts/index',
    'addBankAccount': 'api/bankAccounts/store',
    'updateBankAccount': 'api/bankAccounts/update',
    'deleteBankAccount': 'api/bankAccounts/destroy/',

    'getAppMessages': 'api/appMessages/index',
    'showAppMessage': 'api/appMessages/show/',

    'getCities': 'api/cities/index',
    'addCity': 'api/cities/store',
    'updateCity': 'api/cities/update',
    'deleteCity': 'api/cities/destroy/',

    'getCategories': 'api/categories/index/',
    'showCategory': 'api/categories/show/',
    'addCategory': 'api/categories/store',
    'updateCategory': 'api/categories/update',
    'deleteCategory': 'api/categories/destroy/',

    'getSliders': 'api/sliders/getAllSliders',
    'changeSliderStatus': 'api/sliders/changeStatus/',
    'addSlider': 'api/sliders/store',
    'updateSlider': 'api/sliders/update',
    'deleteSlider': 'api/sliders/destroy/',

    'getAdvertisements': 'api/advertisements/index',
    'getDeletedAdvertisements': 'api/advertisements/deletedIndex',
    'showAdvertisement': 'api/advertisements/show/',
    'updateAdvertisement': 'api/advertisements/update',
    'deleteAdvertisement': 'api/advertisements/destroy/',

    'deleteComment': 'api/comments/adminDestroy/',

    'getAllConversations': 'api/chat/getAllConversations',
    'showConversation': 'api/chat/showConversation/',
    'sendMessage': 'api/chat/sendAdminMessage',

    'getUsers': 'api/users/index',
    'addUser': 'api/users/store',
    'updateUser': 'api/users/update',
    'changeUserStatus': 'api/users/changeStatus/',
    'deleteUser': 'api/users/delete',
    'reorderCategories': 'api/reorder-categories',
    'getComplaints': 'api/getComplaints',
    'closeComplaint': 'api/closeComplaint',

};
