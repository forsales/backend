/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import App from './App.vue'

import {appUrl} from './components/config/apiConfig'
axios.defaults.baseURL = appUrl;

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate);

import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
Vue.use(VueSidebarMenu);

import VueNavigationBar from 'vue-navigation-bar'
import 'vue-navigation-bar/dist/vue-navigation-bar.css'
Vue.component('vue-navigation-bar', VueNavigationBar);

// Start Buefy Framework
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
Vue.use(Buefy);

import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2,{timer: 5000});

import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
Vue.use(Loading);

import Vue from 'vue'

import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import {router} from './router'


const app = new Vue({
    el: '#app',
    render: h => h(App),
    router,
});
