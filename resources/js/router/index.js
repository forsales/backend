import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);
import login from '../components/login'
import Container from '../Container.vue'
import home from '../components/home'
import updateProfile from '../components/admins/updateProfile.vue'
import addAdmin from '../components/admins/addAdmin.vue'
import aboutApplication from '../components/applicationInfo/aboutApplication.vue'
import bankAccounts from '../components/applicationInfo/BankAccounts.vue'
import termsAndConditions from '../components/applicationInfo/termsAndConditions.vue'
import usagePolicy from '../components/applicationInfo/usagePolicy.vue'
import bannedAds from '../components/applicationInfo/bannedAds.vue'
import appInfo from '../components/applicationInfo/appInfo.vue'
import appMessages from '../components/appMessages/index.vue'
import appMessage from '../components/appMessages/show.vue'
import cities from '../components/cities/index.vue'
import sliders from '../components/sliders/index.vue'
import advertisements from '../components/advertisements/index.vue'
import deletedAdvertisements from '../components/advertisements/deleted.vue'
import advertisement from '../components/advertisements/show.vue'
import categories from '../components/categories/index.vue'
import category from '../components/categories/show.vue'
import conversations from '../components/conversations/index.vue'
import conversation from '../components/conversations/show.vue'
import users from '../components/users/index.vue'
import complaints from '../components/complaints/index'

export const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/admin/login',
            name: 'login',
            component: login,
            meta:{ checkLogin: true },
        },
        {
            path: '/admin/',
            component: Container,
            meta:{ userAuth: true },
            children:[
                {
                    path: '/admin/',
                    component: home,
                },
                {
                    path: '/admin/home',
                    name: 'Home',
                    component: home,
                },
                {
                    path: '/admin/updateProfile',
                    name: 'updateProfile',
                    component: updateProfile,
                },
                {
                    path: '/admin/addAdmin',
                    name: 'addAdmin',
                    component: addAdmin,
                },
                {
                    path: '/admin/cities',
                    name: 'cities',
                    component: cities,
                },
                {
                    path: '/admin/complaints',
                    name: 'complaints',
                    component: complaints,
                },
                {
                    path: '/admin/sliders',
                    name: 'sliders',
                    component: sliders,
                },
                {
                    path: '/admin/aboutApp',
                    name: 'aboutApplication',
                    component: aboutApplication,
                },
                {
                    path: '/admin/bankAccounts',
                    name: 'bankAccounts',
                    component: bankAccounts,
                },
                {
                    path: '/admin/terms-conditions',
                    name: 'termsAndConditions',
                    component: termsAndConditions,
                },
                {
                    path: '/admin/usagePolicy',
                    name: 'usagePolicy',
                    component: usagePolicy,
                },
                {
                    path: '/admin/bannedAds',
                    name: 'bannedAds',
                    component: bannedAds,
                },
                {
                    path: '/admin/appInfo',
                    name: 'appInfo',
                    component: appInfo,
                },
                {
                    path: '/admin/appMessages',
                    name: 'appMessages',
                    component: appMessages,
                },
                {
                    path: '/admin/appMessages/show/:appMessage_id',
                    name: 'appMessage',
                    component: appMessage,
                },
                {
                    path: '/admin/advertisements',
                    name: 'advertisements',
                    component: advertisements,
                },
                {
                    path: '/admin/advertisements/deleted',
                    name: 'deletedAdvertisements',
                    component: deletedAdvertisements,
                },
                {
                    path: '/admin/advertisements/show/:advertisement_id',
                    name: 'advertisement',
                    component: advertisement,
                },
                {
                    path: '/admin/categories',
                    name: 'categories',
                    component: categories,
                },
                {
                    path: '/admin/categories/show/:category_id',
                    name: 'category',
                    component: category,
                },
                {
                    path: '/admin/conversations',
                    name: 'conversations',
                    component: conversations,
                },
                {
                    path: '/admin/conversations/show/:conversation_id',
                    name: 'conversation',
                    component: conversation,
                },
                {
                    path: '/admin/users',
                    name: 'users',
                    component: users,
                },
            ]
        },
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.userAuth)) {
        if (!localStorage.getItem('userAuth')) {
            router.push({path: '/admin/login'});
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.checkLogin)) {
        if (localStorage.getItem('userAuth')) {
            router.push({path: '/admin/home'});
        } else {
            next()
        }
    }
    else {
        next()
    }
});
