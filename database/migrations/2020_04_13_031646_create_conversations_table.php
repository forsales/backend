<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('advertisement_id')->nullable();
            $table->foreign('advertisement_id')->references('id')->on('advertisements')->onDelete('set null');
            $table->unsignedInteger('user1_id')->nullable();
            $table->unsignedInteger('user2_id')->nullable();
            $table->foreign('user1_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('user2_id')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations');
    }
}
