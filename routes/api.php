<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace'=>'Api'],function() {

    Route::group(['middleware'=>'auth:admin'],function() {
        Route::get('/home/index', "HomeController@index");

        Route::get('/admins/show', "AdminController@show");
        Route::post('/admins/store', "AdminController@store");
        Route::post('/admins/update', "AdminController@update");
        Route::post('/admins/updatePassword', "AdminController@updatePassword");

        Route::post('/settings/update', "SettingController@update");

        Route::post('/bankAccounts/store', "BankAccountController@store");
        Route::post('/bankAccounts/update', "BankAccountController@update");
        Route::delete('/bankAccounts/destroy/{bankAccount_id}', "BankAccountController@destroy");

        Route::get('/appMessages/index', "AppMessageController@index");
        Route::get('/appMessages/show/{appMessage_id}', "AppMessageController@show");

        Route::post('/cities/store', "CityController@store");
        Route::post('/cities/update', "CityController@update");
        Route::delete('/cities/destroy/{city_id}', "CityController@destroy");

        Route::get('/advertisements/deletedIndex', "AdvertisementController@deletedIndex");

        Route::delete('/comments/adminDestroy/{comment_id}', "CommentController@adminDestroy");

        Route::post('/categories/store', "CategoryController@store");
        Route::post('/categories/update', "CategoryController@update");
        Route::delete('/categories/destroy/{category_id}', "CategoryController@destroy");
        Route::post('/reorder-categories', "CategoryController@reorderCategories");

        Route::get('/sliders/getAllSliders', "SliderController@getAllSliders");
        Route::get('/sliders/changeStatus/{slider_id}', "SliderController@changeStatus");
        Route::post('/sliders/store', "SliderController@store");
        Route::post('/sliders/update', "SliderController@update");
        Route::delete('/sliders/destroy/{slider_id}', "SliderController@destroy");

        Route::get('/chat/getAllConversations', "ChatController@getAllConversations");
        Route::get('/chat/showConversation/{conversation_id}', "ChatController@showConversation");
        Route::post('/chat/sendAdminMessage', "ChatController@sendAdminMessage");

        Route::get('/users/index', "UserController@index");
        Route::post('/users/store', "UserController@store");
        Route::post('/users/delete', "UserController@destroy");
        Route::get('/users/changeStatus/{user_id}', "UserController@changeStatus");

        Route::get('getComplaints', "ComplaintsController@index");
        Route::post('closeComplaint', "ComplaintsController@closeComplaint");
    });

    Route::group(['middleware'=>'checkAuth'],function() {
        Route::post('/users/blockUser', "UserController@blockUser");
        Route::post('/users/unblockUser', "UserController@unblockUser");
        Route::get('/users/blockedUsers', "UserController@blockedUsers");
        Route::post('/users/newComplaint', "UserController@storeComplaint")->middleware(['throttle:10,1']);
        Route::post('/users/update', "UserController@update");
        Route::post('/users/updatePassword', "UserController@updatePassword");
        Route::delete('/users/media', "UserController@deleteMedia");

        Route::post('/advertisements/store', "AdvertisementController@store");
        Route::post('/advertisements/update', "AdvertisementController@update");
        Route::delete('/advertisements/destroy/{advertisement_id}', "AdvertisementController@destroy");

        Route::post('/comments/store', "CommentController@store");
        Route::delete('/comments/destroy/{comment_id}', "CommentController@destroy");

        Route::Resource('favourites', "FavouriteController",['except' => ['create','edit','update','show']]);

        Route::post('/appMessages/store', "AppMessageController@store");

        Route::get('/chat/conversations/index', "ChatController@index");
        Route::get('/chat/conversations/show/{conversation_id}', "ChatController@show");
        Route::post('/chat/conversations/store', "ChatController@store");
        Route::post('/chat/messages/store', "ChatController@sendMessage");

    });
    Route::get('/users/show', "UserController@show");
    Route::post('/users/registration', "UserController@registration");
    Route::get('/users/getUser/{mobile}', "UserController@getUserByMobile");
    Route::post('/users/verification', "UserController@verification");

    Route::get('/sliders/index', "SliderController@index");

    Route::get('/cities/index', "CityController@index");

    Route::get('/categories/index/{type?}', "CategoryController@index");
    Route::get('/categories/show/{category_id}', "CategoryController@show");

    Route::get('/advertisements/index', "AdvertisementController@index");
    Route::get('/advertisements/show/{advertisement_id}', "AdvertisementController@show");

    Route::get('/comments/index/{advertisement_id}', "CommentController@index");

    Route::get('/settings/index', "SettingController@index");

    Route::get('/bankAccounts/index', "BankAccountController@index");

    Route::post('/login', "UserController@login");


});
