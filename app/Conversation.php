<?php

namespace App;

use App\Scopes\FilterByBlockedScope;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = [
        'advertisement_id','user1_id','user2_id','created_at'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new FilterByBlockedScope('user1_id', 'user2_id'));
    }
    public function advertisement(){
        return $this->belongsTo(Advertisement::class);
    }

    public function user1(){
        return $this->belongsTo(User::class,'user1_id');
    }

    public function user2(){
        return $this->belongsTo(User::class,'user2_id');
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public static function createIfNotExist($advertisement_id,$sender_id,$receiver_id) {
        $conversation = Conversation::where('advertisement_id',$advertisement_id)
            ->whereIn('user1_id', [$sender_id, $receiver_id])
            ->whereIn('user2_id', [$sender_id, $receiver_id])
            ->first();

        if ($conversation) {
            return $conversation;
        } else {
            $conversation = Conversation::create([
                'advertisement_id' => $advertisement_id,
                'user1_id' => $sender_id,
                'user2_id' => $receiver_id
            ]);
            return $conversation;
        }
    }

    public static function rules(){
        $rules =  [
            'advertisement_id' => 'required|exists:advertisements,id',
        ];
        return $rules;
    }

    public static function validationMessages() {
        $validationMessages = [
            'advertisement_id.required' => 'من فضلك قم بإدخال رقم الإعلان',
            'advertisement_id.exists' => 'هذا الإعلان غير موجود'
        ];
        return $validationMessages;
    }
}
