<?php

namespace App\Models;

use App\Advertisement;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $table    =   'complaints';
    protected $guarded  =   ['id'];
    protected $appends  =   ['topic'];

    ### Status ###

    const Pending = 0;
    const Closed  = 1;

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getTopicAttribute() {
        switch ($this->type) {
            case '1' :
                return $this->reportedUser;
            case '2' :
                return  $this->advertisement;
            default:
                return $this->user();
        }
    }

    public function reportedUser() {
        return $this->belongsTo(User::class, 'type_id', 'id');
    }
    public function advertisement() {
        return $this->belongsTo(Advertisement::class, 'type_id', 'id');
    }
}
