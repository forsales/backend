<?php


if( !function_exists("public_production")) {
    function public_production() {
        return env('APP_ENV') == 'production' ? '/home1/forsatmy/public_html' : public_path();
    }
}
