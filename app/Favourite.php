<?php

namespace App;

use App\Scopes\FilterByBlockedScope;
use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $fillable = [
        'user_id', 'advertisement_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function advertisement(){
        return $this->belongsTo(Advertisement::class);
    }

    public function rules(){
        $rules =  [
            'advertisement_id' => 'required|exists:advertisements,id',
        ];
        return $rules;
    }

    public static function validationMessages() {
        $validationMessages = [
            'advertisement_id.required' => 'من فضلك قم بإدخال رقم الإعلان',
            'advertisement_id.exists' => 'هذا الإعلان غير موجود'
        ];
        return $validationMessages;
    }
}
