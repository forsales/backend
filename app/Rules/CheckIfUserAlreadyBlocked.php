<?php

namespace App\Rules;

use App\User;
use Illuminate\Contracts\Validation\Rule;

class CheckIfUserAlreadyBlocked implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $exists = auth('api')
            ->user()
            ->blockedUsers()
            ->where('blocked_id', $value)
            ->exists();
        return auth('api')->id() == $value ? false :!$exists;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'تم حذر هذا المستخدم من قبل';
    }
}
