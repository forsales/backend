<?php


namespace App\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class FilterByBlockedScope implements Scope
{
    protected $key = 'user_id';
    protected $secColumn;

    public function __construct($key=null,$secColumn=null)
    {
        if($key) { $this->key = $key; }
        $this->secColumn = $secColumn;
    }

    public function apply(Builder $builder, Model $model)
    {
        $user = auth('api')->user();

        if($user) {
            $builder->where(function ($query)use($user){
                $blocked    =   $user->blockedUsers()->pluck('id');
               $query->whereNotIn($this->key , $blocked)
                   ->when($this->secColumn, function ($query) use($blocked){
                       $query->WhereNotIn($this->secColumn, $blocked);
                   });
            });
        }

    }
}
