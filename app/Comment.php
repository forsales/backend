<?php

namespace App;

use App\Scopes\FilterByBlockedScope;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'comment','user_id', 'advertisement_id'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterByBlockedScope());
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function advertisement(){
        return $this->belongsTo(Advertisement::class);
    }

    public function rules(){
        $rules =  [
            'comment' => 'required',
            'advertisement_id' => 'required|exists:advertisements,id',
        ];
        return $rules;
    }

    public static function validationMessages() {
        $validationMessages = [
            'comment.required' => 'من فضلك قم بإدخال نص التعليق',
            'advertisement_id.required' => 'من فضلك قم بإدخال رقم الإعلان',
            'advertisement_id.exists' => 'هذا الإعلان غير موجود'
        ];
        return $validationMessages;
    }
}
