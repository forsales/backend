<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Slider extends Model
{
    protected $fillable = [
        'type','photo','link','status','advertisement_id',
    ];

    const ACTIVE = 0;
    const NOTACTIVE = 1;

    public function advertisement(){
        return $this->belongsTo(Advertisement::class);
    }

    public function isActive()
    {
        return $this->status == static::ACTIVE;
    }

    public function rules(){
        $rules =  [
            'type' => 'required',
            'photo' => 'required',
            'link' => [ Rule::requiredIf(function () {
                return request()->input('type') != 2 && !request()->has('advertisement_id');
            }),'url'],
            'advertisement_id' => [ 'bail' , Rule::requiredIf(function () {
            return request()->input('type') != 2 && !request()->has('link');
            }),'exists:advertisements,id'],
        ];
        return $rules;
    }

    public function updateRules(){
        $rules =  [
            'type' => 'required',
            'slider_id' => 'required'
        ];
        return $rules;
    }

    public function validationMessages() {
        $validationMessages = [
            'type.required' => 'من فضلك قم بإدخال نوع الإعلان الدعائي',
            'photo.required' => 'من فضلك قم بإدخال صورة الإعلان الدعائي',
            'link.required_if' => 'من فضلك قم بإدخال الرابط أو رقم الإعلان',
            'link.url' => 'الرابط غير صالح',
            'advertisement_id.required_if' => 'من فضلك قم بإدخال الرابط أو رقم الإعلان',
            'advertisement_id.exists' => 'هذا الإعلان غير موجود، برجاء',
        ];
        return $validationMessages;
    }
}
