<?php

namespace App;

use App\Scopes\FilterByBlockedScope;
use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $fillable = [
        'mobile', 'city_id','title','description','video','address','latitude',
        'longitude','status','delete_reason','user_id','category_id', 'youtube_link'
    ];

    const filters = ['id','mobile','city_id','title','description','category_id'];

    const ACTIVE = 0;
    const DELETED = 1;

    public static $types = [
        0 => 'active',
        1 => 'deleted',
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new FilterByBlockedScope());
    }

    public function favourites(){
        return $this->hasMany(Favourite::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function advertisementPhotos(){
        return $this->hasMany(AdvertisementPhoto::class);
    }

    public function conversations(){
        return $this->hasMany(Conversation::class);
    }

    public function slider(){
        return $this->hasOne(Slider::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }


    public function advertisement_status()
    {
        $advertisement_status = $this->status;
        switch ($advertisement_status) {
            case 0:
                return 'active';
                break;
            case 1:
                return 'deleted';
                break;
        }
    }

    /**
     * Determine if the advertisement is active or not.
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->status == static::ACTIVE;
    }

    public function rules(){
        $rules =  [
            'mobile' => 'required',
            'city_id' => 'required|exists:cities,id',
            'title' => 'required',
            'category_id' => 'required|exists:categories,id',
        ];
        return $rules;
    }

    public function updateRules(){
        $rules =  [
            'advertisement_id' => 'required|exists:advertisements,id',
            'mobile' => 'required',
            'city_id' => 'required|exists:cities,id',
            'title' => 'required',
            'category_id' => 'required|exists:categories,id',
        ];
        return $rules;
    }

    public function validationMessages() {
        $validationMessages = [
            'advertisement_id.required' => 'من فضلك قم بإدخال رقم الإعلان',
            'advertisement_id.exists' => 'هذا الإعلان غير موجود',
            'mobile.required' => 'من فضلك قم بإدخال رقم الجوال',
            'city_id.required' => 'من فضلك قم بإختيار المدينة',
            'city_id.exists' => 'المدينة غير موجودة، برجاء اعادة الاختيار',
            'title.required' => 'من فضلك قم بإدخال عنوان الإعلان',
            'category_id.required' => 'من فضلك قم بإختيار قسم الإعلان',
            'category_id.exists' => 'قسم الإعلان غير موجود، برجاء إعادة الاختيار',
        ];
        return $validationMessages;
    }
}
