<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementPhoto extends Model
{
    protected $fillable = [
        'photo','advertisement_id',
    ];

    public function advertisement(){
        return $this->belongsTo(Advertisement::class);
    }

    public function rules(){
        $rules =  [
            'photo' => 'required',
            'advertisement_id' => 'required|exists:advertisements,id',
        ];
        return $rules;
    }
}
