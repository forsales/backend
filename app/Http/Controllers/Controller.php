<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function apiResponse($code,$message=""){

        return response()->json($code['message'],$code["status"]);
    }

    public function photoUploader($file, $folder = null)
    {
        $storagePath = Storage::disk('public_uploads')->put($folder, $file);

        $path = 'images/'.$folder.'/'.basename($storagePath);

        return $path;
    }

//    public function uploadBase64Image($file,$folder){
//        $image = base64_decode(str_replace('data:image/png;base64,', '', $image));
//        $image = base64_decode($file);
//        $storagePath = Storage::disk('public_uploads')->put($folder, $image);
//        $path = 'images/'.$folder.'/'.basename($storagePath);
//        return $path;
//        $name = $path."-".time().".png";
//        $destinationPath = public_path("/images/".$path."/" . $name);
//        file_put_contents($destinationPath, $image);
//        return "images/".$path."/" . $name;
//    }

}
