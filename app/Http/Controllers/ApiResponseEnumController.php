<?php

namespace App\Http\Controllers;


class ApiResponseEnumController
{
    const SUCCESS = 200;
    const AUTHENTICATION_ERROR = 401;
    const VALIDATION_ERROR = 402;
    const ALREADY_THERE = 403;
    const NOT_FOUND = 404;
}
