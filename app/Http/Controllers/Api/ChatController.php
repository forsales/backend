<?php

namespace App\Http\Controllers\Api;

use App\Advertisement;
use App\Conversation;
use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    public function sendMessage(Request $request)
    {
        $message = new Message();
        $validator = Validator::make($request->all(),$message->rules(),$message->validationMessages());
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }
        $inputs = $request->all();
        $conversation = Conversation::query()->find($inputs['conversation_id']);
        $user = auth('api')->user();
        $inputs['sender_id'] = $user->id;
        if ($inputs['sender_id'] == $conversation->user1_id) {
            $inputs['receiver_id'] = $conversation->user2_id;
        } else if ($inputs['sender_id'] == $conversation->user2_id) {
            $inputs['receiver_id'] = $conversation->user1_id;
        } else {
            return response()->json(['message'=>'لم يتم العثور علي المحادثة'],ApiResponseEnumController::NOT_FOUND);
        }

        if (isset($inputs['message_photo'])) {
            $photo = $this->photoUploader($request->file('message_photo'), 'chat');
            unset($inputs['message_photo']);
            $inputs['message_photo'] = $photo;
        }

        $message->create($inputs);

        $conversation->update(['created_at' => \Carbon\Carbon::now()]);

        $conversation = Conversation::with('advertisement','user1','user2','messages','messages.sender','messages.receiver')
            ->find($inputs['conversation_id']);

        return $conversation;
    }

    public function index() {

        $user = auth('api')->user();

        $conversations = $user->conversations();

        foreach ($conversations as $key => $conversation) {
            $unseenMessagesCount = Message::where('conversation_id',$conversation->id)
                ->where('receiver_id',auth('api')->id())
                ->where('seen',Message::UNSEEN)
                ->count();

            $conversations[$key]->unseenMessagesCount = $unseenMessagesCount;
        }

        return $conversations;
    }

    public function show($conversation_id)
    {
        $conversation = Conversation::with('advertisement','user1','user2','messages','messages.sender','messages.receiver')
            ->find($conversation_id);

        if ($conversation) {
            $this->messagesSeen($conversation->id);

            return $conversation;
        } else {
            return response()->json(['message'=>'لم يتم العثور علي المحادثة'],ApiResponseEnumController::NOT_FOUND);
        }
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(),Conversation::rules(),Conversation::validationMessages());
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $advertisement = Advertisement::find($request->advertisement_id);

        $sender_id = auth('api')->id();
        $receiver_id = $advertisement->user_id;
        if ($sender_id != $receiver_id) {
            $conversation = Conversation::createIfNotExist($advertisement->id,$sender_id,$receiver_id);

            $conversation = Conversation::with('advertisement','user1','user2','messages','messages.sender','messages.receiver')
                ->find($conversation->id);

            return $conversation;
        } else {
            return response()->json(['message'=>'لم يتم العثور علي الإعلان'],ApiResponseEnumController::NOT_FOUND);
        }

    }

    public function messagesSeen($conversation_id) {
        Message::where('conversation_id',$conversation_id)
            ->where('receiver_id',auth('api')->id())
            ->where('seen',Message::UNSEEN)
            ->update(['seen' => Message::SEEN]);
    }


    //admin functions
    public function getAllConversations()
    {
        return Conversation::with('advertisement','advertisement.user','user1','user2')
            ->whereHas('messages')
            ->latest()
            ->get();
    }

    public function showConversation($conversation_id)
    {
        $conversation = Conversation::with('advertisement','advertisement.user','messages','messages.sender','messages.receiver')
            ->find($conversation_id);

        if ($conversation) {
            return $conversation;
        } else {
            return response()->json(['message'=>'لم يتم العثور علي المحادثة'],ApiResponseEnumController::NOT_FOUND);
        }
    }

    public function sendAdminMessage(Request $request)
    {
        $message = new Message();
        $validator = Validator::make($request->all(),$message->rules(),$message->validationMessages());
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $inputs = $request->all();
        $conversation = Conversation::find($inputs['conversation_id']);

        if ($conversation) {
            $message->create($inputs);

            $conversation->update(['created_at' => \Carbon\Carbon::now()]);

            $conversation = Conversation::with('advertisement','advertisement.user','messages','messages.sender','messages.receiver')
                ->find($inputs['conversation_id']);

            return $conversation;
        }else {
            return response()->json(['message'=>'لم يتم العثور علي المحادثة'],ApiResponseEnumController::NOT_FOUND);
        }
    }
}
