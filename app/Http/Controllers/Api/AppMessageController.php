<?php

namespace App\Http\Controllers\Api;

use App\AppMessage;
use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AppMessageController extends Controller
{
    public function index()
    {
        return AppMessage::latest()->get();
    }

    public function show($appMessage_id)
    {
        $appMessage = AppMessage::find($appMessage_id);

        if ($appMessage) {
            $appMessage->update(['seen' => AppMessage::SEEN]);
            return $appMessage;
        } else {
            return response()->json(['message'=>'لم يتم العثور علي الرسالة'],ApiResponseEnumController::NOT_FOUND);
        }
    }

    public function store(Request $request)
    {
        $setting = Setting::first();

        $appMessage = new AppMessage();

        $validator = Validator::make($request->all(),$appMessage->rules(),$appMessage->validationMessages());
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $inputs = $request->all();

        $appMessage = $appMessage->create($inputs);

        if (isset($setting->app_email)) {
            $data = [
                'name' => $inputs['name'],
                'body' => $inputs['message'],
                'email' => $inputs['email']
            ];
            Mail::send('emails.appMessage', $data, function($message) use ($inputs,$setting) {
                $message->to($setting->app_email)
                    ->subject($inputs['subject']);
                $message->from($inputs['email']);
            });
        }

        return $appMessage;
    }
}
