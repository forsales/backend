<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        return Setting::first();
    }

    public function update(Request $request)
    {
        $setting = Setting::first();

        $setting->update($request->all());

        return $setting;
    }
}
