<?php

namespace App\Http\Controllers\Api;

use App\Advertisement;
use App\AdvertisementPhoto;
use App\Category;
use App\Favourite;
use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdvertisementController extends Controller
{
    public function index(Request $request)
    {
        $query = Advertisement::where('status',Advertisement::ACTIVE);

        $advertisement = $this->advertisementFilter($request->query(),$query);

        return $advertisement;
    }

    public function deletedIndex(Request $request)
    {
        $query = Advertisement::where('status',Advertisement::DELETED);

        $advertisement = $this->advertisementFilter($request->query(),$query);

        return $advertisement;
    }

    public function show($advertisement_id)
    {
        $advertisement = Advertisement::with('city','category','category.category','category.category.category','user','user.city','advertisementPhotos','comments','comments.user')
            ->find($advertisement_id);

        if (isset($advertisement)) {
            if (Auth::guard('api')->check()) {
                $favourite = auth('api')->user()->favourites()->where('advertisement_id',$advertisement->id)->first();
                if($favourite) {
                    $advertisement->setAttribute('isFavourite', true);
                } else {
                    $advertisement->setAttribute('isFavourite', false);
                }
            }
            return $advertisement;
        } else {
            return response()->json(['message'=>'لم يتم العثور علي الإعلان'],ApiResponseEnumController::NOT_FOUND);
        }
    }

    public function store(Request $request)
    {
        $advertisement = new Advertisement();
        $validator = Validator::make($request->all(),$advertisement->rules(),$advertisement->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }
        $inputs = $request->all();

        $inputs['user_id'] = auth('api')->id();

        if (isset($inputs['video'])) {
            $videoPath = $this->photoUploader($request->file('video'), 'videos');
            unset($inputs['video']);
            $inputs['video'] = $videoPath;
        }

        $advertisement = $advertisement->create($inputs);

        if (isset($inputs['photos'])) {
            foreach ($inputs['photos'] as $photo) {
                $advertisementPhoto = new AdvertisementPhoto();
                $photoPath = $this->photoUploader($photo, 'advertisements');
                $advertisementPhoto->photo = $photoPath;
                $advertisementPhoto->advertisement_id = $advertisement->id;
                $advertisementPhoto->save();
            }
        }

        return $advertisement;
    }

    public function update(Request $request)
    {
        $newAdvertisement = new Advertisement();

        $validator = Validator::make($request->all(),$newAdvertisement->updateRules(),$newAdvertisement->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $advertisement = Advertisement::find($request->advertisement_id);

        $inputs = $request->all();

        if (isset($inputs['video'])) {
            $videoPath = $this->photoUploader($request->file('video'), 'videos');
            unset($inputs['video']);
            $inputs['video'] = $videoPath;
        }

        if (isset($inputs['photos'])) {
            foreach ($inputs['photos'] as $photo) {
                $advertisementPhoto = new AdvertisementPhoto();
                $photoPath = $this->photoUploader($photo, 'advertisements');
                $advertisementPhoto->photo = $photoPath;
                $advertisementPhoto->advertisement_id = $advertisement->id;
                $advertisementPhoto->save();
            }
        }

        $advertisement->update($inputs);

        return Advertisement::with('city','advertisementPhotos','category','category.category','category.category.category','user','user.city')->find($advertisement->id);
    }

    public function destroy($advertisement_id)
    {
        $advertisement = Advertisement::where('id',$advertisement_id)->first();
        if ($advertisement) {
            $advertisement->update(['status' => Advertisement::DELETED,'delete_reason' => null]);
            return response()->json(['message'=>'تم حذف الإعلان بنجاح'],ApiResponseEnumController::SUCCESS);
        } else {
            return response()->json(['message'=>'لم يتم العثور علي الإعلان'],ApiResponseEnumController::NOT_FOUND);
        }
    }

    public function advertisementFilter($filters,$query)
    {
        // handling all types of filters
        foreach ($filters as $key => $filter) {
            if (in_array($key,Advertisement::filters)) {
                // if filters contains category id
                if ($key == 'category_id') {
                    $categoryIds = $this->getCategryChildsIds($filter);
                    foreach ($categoryIds as $id) {
                        $categoryIds = array_merge($categoryIds,$this->getCategryChildsIds($id));
                    }
                    $categoryIds = array_unique($categoryIds);
                    $query = $query->whereIn($key,$categoryIds);
                } else if ($key == 'title' || $key == 'description') {
                    $query = $query->where($key, 'like', '%'.$filter.'%');
                }else {
                    $query = $query->where($key, $filter);
                }
            }
        }

        if (isset($filters['photo'])) {
            if ($filters['photo'] = '1') {
                $query = $query->whereHas('advertisementPhotos');
            }
        }

        $query = $query->with('city','advertisementPhotos','category','category.category','category.category.category','user','user.city')
            ->orderBy('created_at','desc')->get();

        return $query;
    }

    public function getCategryChildsIds($category_id) {
        $categoryIds = Category::orwhere('id',$category_id)->orwhere('category_id',$category_id)
            ->pluck('id')
            ->toArray();

        return $categoryIds;
    }
}
