<?php

namespace App\Http\Controllers\Api;

use App\Admin;
use App\Advertisement;
use App\AppMessage;
use App\Category;
use App\City;
use App\Conversation;
use App\Http\Controllers\Controller;
use App\Message;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $output['usersCount'] = User::count();
        $output['todayUsersCount'] = User::whereDate('created_at', Carbon::today())->count();
        $output['adminsCount'] = Admin::count();

        $output['todayAdsCount'] = Advertisement::where('status',Advertisement::ACTIVE)->whereDate('created_at', Carbon::today())->count();
        $output['activeAdsCount'] = Advertisement::where('status',Advertisement::ACTIVE)->count();
        $output['deletedAdsCount'] = Advertisement::where('status',Advertisement::DELETED)->count();

        $output['conversationsCount'] = Conversation::count();
        $output['todayConversationsCount'] = Message::whereDate('created_at', Carbon::now())->count();
        $output['appMessagesCount'] = AppMessage::count();
        $output['todayAppMessagesCount'] = AppMessage::whereDate('created_at', Carbon::now())->count();
        $output['unseenAppMessagesCount'] = AppMessage::where('seen',AppMessage::UNSEEN)->count();

        $output['categoriesCount'] = Category::where('type',Category::MAIN_CATEGORY)->count();
        $output['citiesCount'] = City::count();

        return $output;
    }
}
