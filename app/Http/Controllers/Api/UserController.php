<?php

namespace App\Http\Controllers\Api;

use App\Advertisement;
use App\AdvertisementPhoto;
use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlockUserRequest;
use App\Http\Requests\storeComplaintRequest;
use App\Models\Complaint;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client;
use Illuminate\Support\Facades\Storage;

use Firebase\Auth\Token\Verifier;


class UserController extends Controller
{
    public function index()
    {
        $users = User::with('city')->latest()->get();

        return $users;
    }

    public function store(Request $request)
    {
        $user = new User();
        $validator = Validator::make($request->all(),$user->rules(),$user->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }
        $inputs = $request->all();

        $inputs['password'] = Hash::make($inputs['password']);

        $user->create($inputs);

        return User::with('city')->get();
    }

    public function show() {
        $user_id    =   \request()->has('user_id') && \request()->input('user_id') ? \request()->input('user_id') : auth('api')->id();

        if(\auth('api')->check() && \auth('api')->user()->blockedUsers()->where('blocked_id',$user_id)->exists()) {
            return  response()->json(['message'=>'قمت بحظر هذا المستخدم من قبل ..'], ApiResponseEnumController::VALIDATION_ERROR);
        }

        $user = User::with(['city','advertisements' => function ($query) {
            $query->where('status',Advertisement::ACTIVE)->orderByDesc('created_at');
        },'advertisements.city','advertisements.category','advertisements.advertisementPhotos','favourites'])
            ->find($user_id);

        if ($user) {
            return $user;
        } else {
            return response()->json(['message'=>'لم يتم العثور علي المستخدم'],ApiResponseEnumController::NOT_FOUND);
        }
    }

    public function update(Request $request)
    {
        if ($request->adminRequest) {
            $user = User::find($request->user_id);
        } else {
            $user = auth('api')->user();
        }

        $validator = Validator::make($request->all(),$user->updateRules(),$user->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $inputs = $request->all();

        if (isset($inputs['profile_photo'])) {
            $photoPath = $this->photoUploader($request->file('profile_photo'), 'users');
            unset($inputs['profile_photo']);
            $inputs['profile_photo'] = $photoPath;
        }

        if (isset($inputs['cover_photo'])) {
            $photoPath = $this->photoUploader($request->file('cover_photo'), 'users');
            unset($inputs['cover_photo']);
            $inputs['cover_photo'] = $photoPath;
        }

        if ($inputs['mobile']) {
            if ($user->mobile != $inputs['mobile']) {
                $inputs['mobile_verified_at'] = null;
                $inputs['mobileVerified'] = 0;
            }
        }

        $user->update($inputs);

        if ($request->adminRequest) {
            return User::with('city')->latest()->get();
        } else {
            return auth('api')->user();
        }
    }

    public function destroy(Request $request) {
        $user   =   User::query()->find($request->input('id'));
        if($user) {
            $user->delete();
            return response()->json(['status'=> true], ApiResponseEnumController::SUCCESS);
        } else {
            return response()->json(['status'=> false], ApiResponseEnumController::NOT_FOUND);
        }
    }

    public function Registration(Request $request)
    {
        $user = new User();
        $validator = Validator::make($request->all(),$user->rules(),$user->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }
        $inputs = $request->all();

        $password = $inputs['password'];

        $inputs['password'] = Hash::make($inputs['password']);

        $user->create($inputs);

        $client = Client::where('password_client', 1)->first();

        $request->request->add([
            'grant_type'    => 'password',
            'client_id'     => $client->id,
            'client_secret' => $client->secret,
            'username'      => $inputs['mobile'],
            'password'      => $password,
            'authProvider'  => 'api',
            'scope'         => null,
        ]);
        $user = User::query()->where('mobile', $inputs['mobile'])->first();
        $tokenResult = $user->createToken('ForSales Personal Access Client');
        $token = $tokenResult->token;
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'user'  =>  $user
        ]);

    }

    public function updatePassword(Request $request)
    {
        $user=auth('api')->user();
        $validator = Validator::make($request->all(),$user->update_password_rules(),$user->validationMessages());
        if($validator->fails()){
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }
        if ((Hash::check($request->oldPassword, $user->password))){
            $user->password=Hash::make($request->newPassword);
            $user->save();
        }
        else {
            return response()->json(['message'=>'كلمة المرور القديمة غير صحيحة، برجاء المحاولة مرة أخري'],ApiResponseEnumController::VALIDATION_ERROR);
        }
        return response()->json(['message'=>'تم تغيير كلمة المرور بنجاح'],ApiResponseEnumController::SUCCESS);
    }

    public function deleteMedia(Request $request)
    {
        $user = auth('api')->user();

        $inputs = $request->query();

        if (isset($inputs['profile_photo'])) {
            if ($inputs['profile_photo'] == 1) {
                $photo_path = str_replace('images/', '', $user->profile_photo);
                Storage::disk('public_uploads')->delete($photo_path);
                $user->update(['profile_photo' => null]);
            }
        }

        if (isset($inputs['cover_photo'])) {
            if ($inputs['cover_photo'] == 1) {
                $photo_path = str_replace('images/', '', $user->cover_photo);
                Storage::disk('public_uploads')->delete($photo_path);
                $user->update(['cover_photo' => null]);
            }
        }

        if (isset($inputs['video'])) {
            if ($inputs['video'] == 1) {
                if (isset($inputs['advertisement_id'])) {
                    $advertisement = Advertisement::find($inputs['advertisement_id']);
                    $video_path = str_replace('images/', '', $advertisement->video);
                    Storage::disk('public_uploads')->delete($video_path);
                    $advertisement->update(['video' => null]);
                } else {
                    return response()->json(['message'=>'لم يتم العثور علي الإعلان'],ApiResponseEnumController::NOT_FOUND);
                }
            }
        }

        if(isset($inputs['advertisement_id'])) {
            $advertisement = Advertisement::find($inputs['advertisement_id']);
            $advertisement->update(['status' => 1]);
        }

        if (isset($inputs['advertisementPhoto_id'])) {
            $advertisementPhoto = AdvertisementPhoto::find($inputs['advertisementPhoto_id']);
            if ($advertisementPhoto) {
                $photo_path = str_replace('images/', '', $advertisementPhoto->photo);
                Storage::disk('public_uploads')->delete($photo_path);
                $advertisementPhoto->delete();
            } else {
                return response()->json(['message'=>'لم يتم العثور علي صورة الإعلان'],ApiResponseEnumController::NOT_FOUND);
            }
        }

        return response()->json(['message'=>'تم الحذف بنجاح'],ApiResponseEnumController::SUCCESS);
    }

    public function getUserByMobile($mobile)
    {
        $user = User::where('mobile',$mobile)->first();
        if ($user) {
            return $user;
        } else {
            return response()->json(['message'=>'لم يتم العثور علي المستخدم'],ApiResponseEnumController::NOT_FOUND);
        }

    }

    public function verification(Request $request)
    {
        $projectId = 'forsales-1457c';

        $verifier = new Verifier($projectId);

        try {
            $verifiedIdToken = $verifier->verifyIdToken($request->token);

            $mobile = $verifiedIdToken->getClaim('phone_number');
            $mobile = preg_replace('/^\+?966|\|966|\D/', '', ($mobile));
            $user = User::where('mobile',$mobile)->first();

            if ($user) {
                $user->mobileVerified = 1;
                $user->mobile_verified_at = Carbon::now('Asia/Riyadh');
                if ($request->newPassword) {
                    $user->password = Hash::make($request->newPassword);
                }
                $user->save();

                if ($request->newPassword) {
                    $client = Client::where('password_client', 1)->first();

                    $request->request->add([
                        'grant_type'    => 'password',
                        'client_id'     => $client->id,
                        'client_secret' => $client->secret,
                        'username'      => $user->mobile,
                        'password'      => $request->newPassword,
                        'authProvider'  => 'api',
                        'scope'         => null,
                    ]);

                    // Fire off the internal request.
                    $token = Request::create(
                        'oauth/token',
                        'POST'
                    );
                    return \Route::dispatch($token);
                } else {
                    return $user;
                }
            } else {
                return response()->json(['message'=>'رمز خاطئ، برجاء المحاولة مرة أخري'],ApiResponseEnumController::VALIDATION_ERROR);
            }

        } catch (\Throwable $e) {
            return response()->json(['message'=>'رمز خاطئ، برجاء المحاولة مرة أخري'],ApiResponseEnumController::VALIDATION_ERROR);
        }
    }

    public function changeStatus($user_id)
    {
        $user = User::find($user_id);

        if ($user->isActive()) {
            $user->update(['status' => User::DEACTIVE]);
        } else {
            $user->update(['status' => User::ACTIVE]);
        }

        return User::with('city')->latest()->get();
    }

    public function login(Request $request)
    {
        $request->validate([
            'mobile' => 'required',
            'password' => 'required'
        ]);
        $credentials = request(['mobile', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('ForSales Personal Access Client');
        $token = $tokenResult->token;
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'user'  =>  $request->user()
        ]);
    }

    public function blockUser(BlockUserRequest $request) {
        auth('api')->user()->blockedUsers()->attach($request->input('user_id'));
        return response()->json(['message'=>'تم حظر المستخدم ..']);
    }

    public function unblockUser(Request $request) {
        auth('api')->user()->blockedUsers()->detach($request->input('user_id'));
        return response()->json(['message'=>'تم الغاء حظر المستخدم ..']);
    }

    public function blockedUsers() {
        $blocked    =   auth('api')->user()->blockedUsers()->select('id', 'name', 'profile_photo')->get();
        return response()->json(['blocked_users'=>$blocked]);
    }

    public function storeComplaint(storeComplaintRequest $request) {
        Complaint::query()->create([
            'user_id'   =>  \auth('api')->id(),
            'descriptions'  =>  $request->input('description'),
            'type' =>  $request->input('type'),
            'type_id' =>  $request->input('type_id')
        ]);
        return response()->json(['message'=>'تم تقديم الشكوي وجاري مراجعتها ..']);
    }
}
