<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index()
    {
        return City::all();
    }

    public function store(Request $request)
    {
        $city = new City();

        $inputs = $request->all();

        $city->create($inputs);

        return City::all();
    }

    public function update(Request $request)
    {
        $city = City::find($request->city_id);

        $inputs = $request->all();

        $city->update($inputs);

        return City::all();
    }

    public function destroy($city_id) {
        $city = City::find($city_id);

        $city->delete();

        return City::all();
    }
}
