<?php

namespace App\Http\Controllers\Api;

use App\Advertisement;
use App\Category;
use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\If_;

class CategoryController extends Controller
{
    public function index($type = null)
    {
        If (isset($type)) {
            return Category::where('type',$type)->with('categories','categories.categories')->get();
        } else {
            return Category::with('categories','categories.categories')->get();
        }
    }

    public function show($category_id)
    {
        $category = Category::find($category_id);
        if (isset($category)) {
            $subCategories = Category::where('category_id',$category_id)
                ->with('categories')
                ->orderBy('order')
                ->get()
                ->toArray();

            $result = ['category' => $category,'subCategories' => $subCategories];

            return $result;

        } else {
            return response()->json(['message'=>'لم يتم العثور علي قسم الإعلان'],ApiResponseEnumController::NOT_FOUND);
        }
    }

    public function store(Request $request)
    {
        $category = new Category();
        $validator = Validator::make($request->all(),$category->rules(),$category->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }
        $inputs = $request->all();
        $inputs = array_map(function($input){return $input == "null" ? null : $input;}, $inputs);
        if (isset($inputs['photo']) && $inputs['photo'] != "undefined") {
            $photo = $this->photoUploader($request->file('photo'), 'categories');
            unset($inputs['photo']);
            $inputs['photo'] = $photo;
        } else {
            $inputs['photo'] = 'images/forsales/app.png';
        }

        $category = $category->create($inputs);

        if ($category->type != Category::MAIN_CATEGORY) {
            return Category::where('category_id',$category->category_id)->with('categories')->get();
        } else {
            return Category::where('type',Category::MAIN_CATEGORY)->with('categories','categories.categories')->get();
        }
    }

    public function update(Request $request)
    {
        $category = Category::find($request->id);

        $validator = Validator::make($request->all(),$category->updateRules(),$category->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $inputs = $request->all();

        if (isset($inputs['photo']) && $inputs['photo'] != null) {
            $photoPath = $this->photoUploader($request->file('photo'), 'categories');
            unset($inputs['photo']);
            $inputs['photo'] = $photoPath;
        }

        $category->update($inputs);

        if ($category->type != Category::MAIN_CATEGORY) {
            return Category::where('category_id',$category->category_id)->with('categories')->get();
        } else {
            return Category::where('type',Category::MAIN_CATEGORY)->with('categories','categories.categories')->get();
        }
    }

    public function destroy($category_id) {
        $category = Category::find($category_id);

        $type = $category->type;

        $category->delete();

        if ($type != Category::MAIN_CATEGORY) {
            $categories = Category::where('category_id',$category->category_id)->with('categories')->get();
        } else {
            $categories = Category::where('type',Category::MAIN_CATEGORY)->with('categories','categories.categories')->get();
        }

        return $categories;
    }

    public function reorderCategories(Request $request) {
        foreach($request->input('orders') as $order) {
            Category::query()->find($order['id'])->update(['order'=>$order['order']]);
        }
        $ids = collect(json_decode(json_encode(request()->input('orders'))))->pluck('id');
        return Category::query()
            ->whereIn('id' , $ids)
            ->get();
    }
}
