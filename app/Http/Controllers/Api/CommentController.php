<?php

namespace App\Http\Controllers\Api;

use App\Advertisement;
use App\Comment;
use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function index($advertisement_id)
    {
        return Comment::where('advertisement_id',$advertisement_id)->with('user')->get();
    }

    public function store(Request $request)
    {
        $comment = new Comment();
        $validator = Validator::make($request->all(),$comment->rules(),$comment->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }
        $inputs = $request->all();

        $inputs['user_id'] = auth('api')->id();

        $comment = $comment->create($inputs);

        return $comment;

    }

    public function destroy($comment_id)
    {
        $comment = Comment::where('user_id',auth('api')->id())->where('id',$comment_id)->first();

        if ($comment) {
            $comment->delete();
            return response()->json(['message'=>'تم حذف التعليق بنجاح'],ApiResponseEnumController::SUCCESS);
        } else {
            return response()->json(['message'=>'لم يتم العثور علي التعليق'],ApiResponseEnumController::NOT_FOUND);
        }

    }

    public function adminDestroy($comment_id)
    {
        $comment = Comment::find($comment_id);

        if ($comment) {
            $advertisement = Advertisement::find($comment->advertisement_id);
            $comment->delete();
            return $advertisement->comments;
        } else {
            return response()->json(['message'=>'لم يتم العثور علي التعليق'],ApiResponseEnumController::NOT_FOUND);
        }
    }
}
