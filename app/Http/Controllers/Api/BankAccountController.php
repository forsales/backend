<?php

namespace App\Http\Controllers\Api;

use App\BankAccount;
use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BankAccountController extends Controller
{
    public function index()
    {
        return BankAccount::all();
    }

    public function store(Request $request)
    {
        $bankAccount = new BankAccount();

        $validator = Validator::make($request->all(),$bankAccount->rules(),$bankAccount->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $inputs = $request->all();

        if (isset($inputs['bank_photo'])) {
            $photoPath = $this->photoUploader($request->file('bank_photo'), 'banks');
            unset($inputs['bank_photo']);
            $inputs['bank_photo'] = $photoPath;
        }

        $bankAccount->create($inputs);

        return BankAccount::all();
    }

    public function update(Request $request)
    {
        $bankAccount = BankAccount::find($request->bankAccount_id);

        $validator = Validator::make($request->all(),$bankAccount->rules(),$bankAccount->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $inputs = $request->all();

        if (isset($inputs['bank_photo']) && $inputs['bank_photo'] != null) {
            $photoPath = $this->photoUploader($request->file('bank_photo'), 'banks');
            unset($inputs['bank_photo']);
            $inputs['bank_photo'] = $photoPath;
        }

        $bankAccount->update($inputs);

        return BankAccount::all();
    }

    public function destroy($bankAccount_id) {
        $bankAccount = BankAccount::find($bankAccount_id);

        $bankAccount->delete();

        return BankAccount::all();
    }
}
