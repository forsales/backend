<?php

namespace App\Http\Controllers\Api;

use App\Advertisement;
use App\Favourite;
use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FavouriteController extends Controller
{
    public function index()
    {
        return auth('api')
            ->user()
            ->favourites()
            ->with('advertisement','advertisement.city','advertisement.advertisementPhotos','advertisement.category')->get();
    }

    public function store(Request $request)
    {
        $favourite = new Favourite();
        $validator = Validator::make($request->all(),$favourite->rules(),$favourite->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }
        $inputs = $request->all();

        $inputs['user_id'] = auth('api')->id();

        $exist = Favourite::where('user_id',$inputs['user_id'])->where('advertisement_id',$inputs['advertisement_id'])->first();

        if($exist) {
            return response()->json(['message'=>'هذا الإعلان موجود في المفضلة من قبل'],ApiResponseEnumController::ALREADY_THERE);
        } else {
            $favourite = $favourite->create($inputs);

            return $favourite;
        }
    }

    public function destroy($advertisement_id)
    {
        $favourite = Favourite::where('user_id',auth('api')->id())
            ->where('advertisement_id',$advertisement_id)->first();

        if ($favourite) {
            $favourite->delete();
            return response()->json(['message'=>'تم حذف المفضلة بنجاح'],ApiResponseEnumController::SUCCESS);
        } else {
            return response()->json(['message'=>'لم يتم العثور علي المفضلة'],ApiResponseEnumController::NOT_FOUND);
        }
    }
}
