<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ComplaintResource;
use App\Models\Complaint;
use Illuminate\Http\Request;

class ComplaintsController extends Controller
{
    public function index() {
        $complaints = Complaint::query()
            ->orderBy('status')
            ->orderByDesc('created_at')
            ->with('user')
            ->get();
        return response()->json(['complaints' => ComplaintResource::collection($complaints)]);
    }

    public function closeComplaint(Request $request) {
        $complaint  =   Complaint::query()->find($request->input('id'));
        if($complaint) {
            $complaint->update(['status' => Complaint::Closed]);
            $complaints = Complaint::query()
                ->orderBy('status')
                ->orderByDesc('created_at')
                ->with('user')
                ->get();
            return response()->json([
                'message'=>'تم اغلاق الشكوي نجاح',
                'complaints'    => ComplaintResource::collection($complaints)
            ]);
        }



        return response()->json(['message'       => 'حدث خطأ اثناء اغلاق الشكوي']);
    }
}
