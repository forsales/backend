<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    //get only active sliders
    public function index()
    {
        return Slider::where('status',Slider::ACTIVE)->with('advertisement')->get();
    }

    //get all sliders
    public function getAllSliders()
    {
        return Slider::with('advertisement')->get();
    }

    public function store(Request $request)
    {
        $slider = new Slider();
        $validator = Validator::make($request->all(),$slider->rules(),$slider->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }
        $inputs = $request->all();

        if (isset($inputs['photo'])) {
            $photo = $this->photoUploader($request->file('photo'), 'sliders');
            unset($inputs['photo']);
            $inputs['photo'] = $photo;
        }

        $slider->create($inputs);

        return  Slider::with('advertisement')->get();
    }

    public function update(Request $request)
    {
        $slider = Slider::find($request->slider_id);

        $validator = Validator::make($request->all(),$slider->updateRules(),$slider->validationMessages());

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $inputs = $request->all();

        if (isset($inputs['photo']) && $inputs['photo'] != null) {
            $photoPath = $this->photoUploader($request->file('photo'), 'categories');
            unset($inputs['photo']);
            $inputs['photo'] = $photoPath;
        }

        $slider->update($inputs);

        return Slider::with('advertisement')->get();
    }

    public function destroy($slider_id) {
        $slider = Slider::find($slider_id);

        $slider->delete();

        return Slider::with('advertisement')->get();
    }

    public function changeStatus($slider_id)
    {
        $slider = Slider::find($slider_id);

        if ($slider->isActive()) {
            $slider->update(['status' => Slider::NOTACTIVE]);
        } else {
            $slider->update(['status' => Slider::ACTIVE]);
        }

        return Slider::with('advertisement')->get();
    }
}
