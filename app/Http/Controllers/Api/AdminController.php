<?php

namespace App\Http\Controllers\Api;

use App\Admin;
use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function show()
    {
        $admin = Admin::find(auth('admin')->id());

        return $admin;
    }

    public function store(Request $request)
    {
        $admin = new Admin();

        $validator = Validator::make($request->all(),$admin->rules(),$admin->validationMessages());
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $inputs = $request->all();

        $inputs['password'] = Hash::make($inputs['password']);

        $admin = $admin->create($inputs);

        return $admin;
    }

    public function updatePassword(Request $request)
    {
        $admin = Admin::find(auth('admin')->id());
        $validator = Validator::make($request->all(),$admin->reset_password_rules(),$admin->validationMessages());
        if($validator->fails()){
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        if ((Hash::check($request->oldPassword, $admin->password))){
            $admin->password = Hash::make($request->newPassword);
            $admin->save();
        }
        else {
            return response()->json(['message'=>'كلمة المرور القديمة غير صحيحة، برجاء المحاولة مرة أخري'],ApiResponseEnumController::VALIDATION_ERROR);
        }
        return response()->json(['message'=>'تم تغيير كلمة المرور بنجاح'],ApiResponseEnumController::SUCCESS);
    }

    public function update(Request $request)
    {
        $admin = Admin::find(auth('admin')->id());

        $validator = Validator::make($request->all(),$admin->updateRules(),$admin->validationMessages());
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first()],ApiResponseEnumController::VALIDATION_ERROR);
        }

        $inputs = $request->all();

        $admin->update($inputs);

        return $admin;
    }
}
