<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ComplaintResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'created_at'    =>  $this->created_at,
            'reporter'      =>  $this->user->name,
            'topic'         =>  $this->topic,
            'descriptions'   =>  $this->descriptions,
            'status'        =>  $this->status,
            'type'          =>  $this->type
        ];
    }
}
