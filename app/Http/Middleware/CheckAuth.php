<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ApiResponseEnumController;
use App\Http\Controllers\Controller;
use Closure;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth('api')->check() || auth('admin')->check()) {
            return $next($request);
        } else {
            return Controller::apiResponse(ApiResponseEnumController::AUTHENTICATION_ERROR);
        }
    }
}
