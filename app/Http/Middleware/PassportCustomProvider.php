<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

class PassportCustomProvider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $params = $request->all();
        if (array_key_exists('authProvider', $params)) {
            if($params['authProvider'] == 'api') {
                Config::set('auth.guards.api.provider', "users");
            } else if ($params['authProvider'] == 'admin') {
                Config::set('auth.guards.api.provider', "admins");
            }
        }
        return $next($request);
    }
}
