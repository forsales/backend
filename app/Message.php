<?php

namespace App;

use App\Scopes\FilterByBlockedScope;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'sender_id','receiver_id','conversation_id','message','message_photo','seen'
    ];

    const SEEN = 1;
    const UNSEEN = 0;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new FilterByBlockedScope('sender_id', 'receiver_id'));
    }

    public function sender(){
        return $this->belongsTo(User::class,'sender_id');
    }

    public function receiver(){
        return $this->belongsTo(User::class,'receiver_id');
    }

    public function conversation(){
        return $this->belongsTo(Conversation::class);
    }

    public function rules(){
        $rules =  [
            'conversation_id' => 'required|exists:conversations,id',
            'message' => 'required_without:message_photo',
            'message_photo' => 'required_without:message',
        ];
        return $rules;
    }

    public function validationMessages() {
        $validationMessages = [
            'conversation_id.required' => 'من فضلك قم بإدخال رمز المحادثة',
            'conversation_id.exists' => 'رمز المحادثة غير موجود، برجاء إعادة الاختيار',
            'message.required_without' => 'من فضلك قم بإدخال نص الرسالة',
        ];
        return $validationMessages;
    }

}
