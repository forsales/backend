<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppMessage extends Model
{
    protected $fillable = [
        'name','email','subject','message','seen','user_id'
    ];

    const SEEN = 1;
    const UNSEEN = 0;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function rules(){
        $rules =  [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        ];
        return $rules;
    }

    public function validationMessages() {
        $validationMessages = [
            'name.required' => 'من فضلك قم بإدخال الاسم',
            'email.required' => 'من فضلك قم بإدخال البريد الالكتروني',
            'email.email' => 'البريد الالكتروني غير صالح',
            'subject.required' => 'من فضلك قم بإدخال عنوان الرسالة',
            'message.required' => 'من فضلك قم بإدخال نص الرسالة',
        ];
        return $validationMessages;
    }
}
