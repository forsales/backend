<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'terms_conditions','usage_policy', 'about_app','banned_ads','app_email',
        'app_mobile','app_store','google_play','commission',
    ];

    public function rules(){
        $rules =  [
            'app_email' => 'email',
            'app_store' => 'nullable|url',
            'google_play' => 'nullable|url',
            'app_mobile' => 'numeric',
        ];
        return $rules;
    }
}
