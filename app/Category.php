<?php

namespace App;

use App\Scopes\OrderByScope;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrderByScope);
    }

    protected $fillable = [
        'name', 'photo','description','type','category_id','order'
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    const MAIN_CATEGORY = 1;
    const SUB_CATEGORY = 2;
    const SUB_OF_SUB_CATEGORY = 3;

    public static $types = [
        1 => 'main_category',
        2 => 'sub_category',
        3 => 'sub_of_sub_category',
    ];

    public function advertisements(){
        return $this->hasMany(Advertisement::class);
    }

    public function categories(){
        return $this->hasMany(Category::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function category_type()
    {
        $category_type = $this->type;
        switch ($category_type) {
            case 1:
                return 'main_category';
                break;
            case 2:
                return 'sub_category';
                break;
            case 3:
                return 'sub_of_sub_category';
                break;
        }
    }

    /**
     * Determine if the category is main or not.
     *
     * @return boolean
     */
    public function isMainCategory()
    {
        return $this->type == static::MAIN_CATEGORY;
    }

    /**
     * Determine if the category is sub or not.
     *
     * @return boolean
     */
    public function isSubCategory()
    {
        return $this->type == static::SUB_CATEGORY;
    }

    /**
     * Determine if the category is sub of sub or not.
     *
     * @return boolean
     */
    public function isSubOfSubCategory()
    {
        return $this->type == static::SUB_OF_SUB_CATEGORY;
    }

    public function rules(){
        $rules =  [
            'name' => 'required',
            'type' => 'required',
            'category_id' => 'required_unless:type,1|exists:categories,id'
        ];
        return $rules;
    }

    public function updateRules(){
        $rules =  [
            'name' => 'required',
        ];
        return $rules;
    }

    public function validationMessages() {
        $validationMessages = [
            'name.required' => 'من فضلك قم بإدخال اسم القسم',
            'type.required' => 'من فضلك قم بإدخال نوع القسم',
            'category_id.required_unless' => 'من فضلك قم بإختيار القسم',
            'category_id.exists' => 'هذا القسم غير موجود',
        ];
        return $validationMessages;
    }

}
