<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $fillable = [
        'bank_name','bank_photo', 'owner_name','account_number','IBAN_number'
    ];

    public function rules(){
        $rules =  [
            'bank_name' => 'required',
            'owner_name' => 'required',
            'account_number' => 'required',
            'IBAN_number' => 'required',
        ];
        return $rules;
    }

    public function validationMessages() {
        $validationMessages = [
            'bank_name.required' => 'من فضلك قم بإدخال اسم البنك',
            'owner_name.required' => 'من فضلك قم بإدخال اسم صاحب الحساب',
            'account_number.required' => 'من فضلك قم بإدخال رقم الحساب',
            'IBAN_number.required' => 'من فضلك قم بإدخال رقم الأيبان',
        ];
        return $validationMessages;
    }
}
