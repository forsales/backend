<?php

namespace App;

use App\Scopes\FilterByBlockedScope;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password','mobile','mobileVerified','mobile_verified_at','address',
        'city_id','description','profile_photo','cover_photo','facebook_link','twitter_link','status'
    ];

    const ACTIVE = 1;
    const DEACTIVE = 0;

    protected $hidden = [
        'password', 'remember_token','updated_at'
    ];

    protected $casts = [
        'mobile_verified_at' => 'datetime'
    ];

    protected $appends  =   ['created'];

    public function isActive()
    {
        return $this->status == static::ACTIVE;
    }

    public function advertisements(){
        return $this->hasMany(Advertisement::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function favourites(){
        $user = auth('api')->user();
        return $this->hasMany(Favourite::class)->whereHas('advertisement', function ($q) use($user){
            $q->whereNotIn('user_id', is_null($user) ? [] : $user->blockedUsers()->pluck('id'));
        })->orderByDesc('created_at');
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function conversations(){
        return Conversation::with('advertisement','user1','user2')
            ->whereHas('messages')
            ->where(function($q) {
                $q->where('user1_id',$this->id)
                    ->orWhere('user2_id', $this->id);
            })
            ->with('advertisement','user1','user2','messages')
            ->latest()
            ->get();
    }


    public function rules(){
        $rules =  [
            'name' => 'required',
            'mobile' => ['required','regex:/^(5)[0-9]{8}$/','unique:users,mobile'],
            'email' => 'nullable|email|unique:users,email',
            'city_id' => 'required|exists:cities,id',
            'password' => 'required|min:6',
            'facebook_link' => 'nullable|url',
            'twitter_link' => 'nullable|url',
        ];
        return $rules;
    }

    public function updateRules(){
        $rules =  [
            'name' => 'required',
            'mobile' => ['required','regex:/^(5)[0-9]{8}$/','unique:users,mobile,'.$this->id],
            'email' => 'nullable|email|unique:users,email,'.$this->id,
            'city_id' => 'required|exists:cities,id',
            'facebook_link' => 'nullable|url',
            'twitter_link' => 'nullable|url',
        ];
        return $rules;
    }

    public function update_password_rules(){
        $rules = [
            'newPassword'     => 'required|min:6|confirmed',
            'oldPassword'     => 'required'
        ];
        return $rules;
    }

    public function reset_password_rules(){
        $rules = [
            'newPassword'     => 'required|min:6|confirmed',
        ];
        return $rules;
    }

    public function validationMessages() {
        $validationMessages = [
            'name.required' => 'من فضلك قم بإدخال الاسم',
            'mobile.required' => 'من فضلك قم بإدخال رقم الجوال',
            'mobile.unique' => 'رقم الجوال موجود بالفعل',
            'email.unique' => 'البريد الالكتروني موجود بالفعل',
            'email.email' => 'البريد الالكتروني غير صالح',
            'city_id.required' => 'من فضلك قم بإختيار المدينة',
            'city_id.exists' => 'المدينة غير موجودة، برجاء اعادة الاختيار',
            'password.required' => 'من فضلك قم بإدخال كلمة المرور',
            'password.min' => 'يجب أن تكون كلمة المرور 6 أحرف على الأقل',
            'newPassword.required' => 'من فضلك قم بإدخال كلمة المرور الجديدة',
            'newPassword.min' => 'يجب أن تكون كلمة المرور الجديدة 6 أحرف على الأقل',
            'newPassword.confirmed' => 'من فضلك قم بإدخال تأكيد كلمة المرور الجديدة',
            'oldPassword.required' => 'من فضلك قم بإدخال كلمة المرور القديمة',
            'facebook_link.url' => 'رابط الفيسبوك غير صالح',
            'twitter_link.url' => 'رابط تويتر غير صالح',
        ];
        return $validationMessages;
    }

    /**
     * Find the user instance for the given mobile number.
     *
     * @param  string  $mobile
     * @return \App\User
     */
    public function findForPassport($mobile)
    {
        return $this->where('mobile', $mobile)->first();
    }

    public function getCreatedAttribute() {
        return Carbon::parse($this->created_at)->toDateString();
    }

    public function blockedUsers() {
        return $this->belongsToMany(User::class, 'users_blocked', 'user_id', 'blocked_id');
    }
}
