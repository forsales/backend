<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rules(){
        $rules =  [
            'name' => 'required',
            'username' => 'required|unique:admins,username',
            'password' => 'required|min:6|confirmed',
        ];
        return $rules;
    }

    public function updateRules(){
        $rules =  [
            'name' => 'required',
            'username' => 'required|unique:admins,username,'.auth('admin')->id(),
        ];
        return $rules;
    }

    public function reset_password_rules(){
        $rules = [
            'oldPassword'     => 'required',
            'newPassword'     => 'required|min:6|confirmed',
        ];
        return $rules;
    }

    public function validationMessages() {
        $validationMessages = [
            'name.required' => 'من فضلك قم بإدخال الاسم',
            'username.required' => 'من فضلك قم بإدخال اسم المستخدم',
            'username.unique' => 'اسم المستخدم موجود بالفعل',
            'password.required' => 'من فضلك قم بإدخال كلمة المرور',
            'password.min' => 'يجب أن تكون كلمة المرور 6 أحرف على الأقل',
            'newPassword.required' => 'من فضلك قم بإدخال كلمة المرور الجديدة',
            'newPassword.min' => 'يجب أن تكون كلمة المرور الجديدة 6 أحرف على الأقل',
            'newPassword.confirmed' => 'من فضلك قم بإدخال تأكيد كلمة المرور الجديدة',
            'oldPassword.required' => 'من فضلك قم بإدخال كلمة المرور القديمة',
        ];
        return $validationMessages;
    }

    /**
     * Find the user instance for the given mobile number.
     *
     * @param  string  $mobile
     * @return \App\User
     */
    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }
}